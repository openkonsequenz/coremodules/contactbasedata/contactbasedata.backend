-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

DELETE FROM tbl_communication;
DELETE FROM tbl_assignment_modul_contact;
DELETE FROM tbl_address;
DELETE FROM tbl_contact_person;
DELETE FROM tbl_external_person;
DELETE FROM tbl_internal_person;
DELETE FROM tbl_company;
DELETE FROM tbl_contact;