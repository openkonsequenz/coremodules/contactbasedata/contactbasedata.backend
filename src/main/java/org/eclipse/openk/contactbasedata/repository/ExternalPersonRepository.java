/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.repository;

import org.eclipse.openk.contactbasedata.model.TblExternalPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface ExternalPersonRepository extends JpaRepository<TblExternalPerson, Long > {
    @Query("select ep from TblExternalPerson ep where ep.contact.uuid = ?1")
    Optional< TblExternalPerson > findByTblContactUuid(final UUID contactUuid );

    Page<TblExternalPerson> findByContact_anonymizedFalseOrContact_anonymizedIsNull(Pageable pageable); // NOSONAR fd 07.02.2020: Method name cannot be changed

}
