/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.mapper;

import org.eclipse.openk.contactbasedata.model.TblCompany;
import org.eclipse.openk.contactbasedata.viewmodel.CompanyDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompanyMapper {
    @Mappings({
            @Mapping( source="contact.uuid", target="contactUuid"),
            @Mapping( source="contact.contactType", target="contactType"),
            @Mapping( source="contact.note", target="contactNote"),
            @Mapping( source="contact.anonymized", target="contactAnonymized"),

    })
    CompanyDto toCompanyDto(TblCompany tblCompany);

    @Mappings({
            @Mapping( source="contactUuid", target="contact.uuid"),
            @Mapping( source="contactType", target="contact.contactType"),
            @Mapping( source="contactNote", target="contact.note"),
            @Mapping( source="contactAnonymized", target="contact.anonymized"),
    })
    TblCompany toTblCompany(CompanyDto companyDto);
}
