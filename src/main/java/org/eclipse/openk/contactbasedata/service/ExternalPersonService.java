/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.constants.Constants;
import org.eclipse.openk.contactbasedata.exceptions.NotFoundException;
import org.eclipse.openk.contactbasedata.mapper.ContactMapper;
import org.eclipse.openk.contactbasedata.mapper.ExternalPersonMapper;
import org.eclipse.openk.contactbasedata.model.TblContact;
import org.eclipse.openk.contactbasedata.model.TblExternalPerson;
import org.eclipse.openk.contactbasedata.repository.ContactRepository;
import org.eclipse.openk.contactbasedata.repository.ExternalPersonRepository;
import org.eclipse.openk.contactbasedata.repository.PersonTypeRepository;
import org.eclipse.openk.contactbasedata.repository.SalutationRepository;
import org.eclipse.openk.contactbasedata.viewmodel.ExternalPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
public class ExternalPersonService {
    @Autowired
    private ExternalPersonRepository externalPersonRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private SalutationRepository salutationRepository;

    @Autowired
    private PersonTypeRepository personTypeRepository;

    @Autowired
    private ExternalPersonMapper externalPersonMapper;

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private BaseContactService baseContactService;

    public ExternalPersonDto findExternalPerson(UUID contactUuid) {
        return externalPersonMapper.toExternalPersonDto(
                externalPersonRepository.findByTblContactUuid(contactUuid).orElseThrow(NotFoundException::new)
        );
    }

    public Page<ExternalPersonDto> findExternalPersons(boolean showAlsoAnonymous, Pageable pageable) {
        return (showAlsoAnonymous
                ? externalPersonRepository.findAll(pageable)
                : externalPersonRepository.findByContact_anonymizedFalseOrContact_anonymizedIsNull(pageable))
                .map(externalPersonMapper::toExternalPersonDto);
    }


    @Transactional
    public ExternalPersonDto insertExternalPerson(ExternalPersonDto externalPersonDto) {
        TblContact contactToSave = new TblContact();
        contactToSave.setUuid(UUID.randomUUID());
        contactToSave.setContactType(Constants.CONTACT_TYPE_EXTERNAL_PERSON);

        TblExternalPerson externalPersonToSave = externalPersonMapper.toTblExternalPerson(externalPersonDto);
        externalPersonToSave.setContact(contactToSave);
        contactRepository.save(externalPersonToSave.getContact());

        setFromExternalPersonDto( externalPersonToSave, externalPersonDto );

        // Then save dependent Model-Object
        return externalPersonMapper.toExternalPersonDto(externalPersonRepository.save(externalPersonToSave));
    }

    @Transactional
    public ExternalPersonDto updateExternalPerson(ExternalPersonDto externalPersonDto){
        TblExternalPerson externalPersonUpdated;

        TblExternalPerson existingExternalPerson = externalPersonRepository
                .findByTblContactUuid(externalPersonDto.getContactUuid())
                .orElseThrow(() -> new NotFoundException("contact.uuid.not.existing"));

        existingExternalPerson.setLastName(externalPersonDto.getLastName());
        existingExternalPerson.setFirstName(externalPersonDto.getFirstName());
        existingExternalPerson.setTitle(externalPersonDto.getTitle());

        setFromExternalPersonDto( existingExternalPerson, externalPersonDto );
        externalPersonUpdated = externalPersonRepository.save(existingExternalPerson);

        return externalPersonMapper.toExternalPersonDto(externalPersonUpdated);
    }

    private void setFromExternalPersonDto( TblExternalPerson destTblExternalPerson, ExternalPersonDto sourceDto ) {

        if( sourceDto.getSalutationUuid() != null ) {
            destTblExternalPerson.setSalutation( salutationRepository
                    .findByUuid(sourceDto.getSalutationUuid())
                    .orElseThrow(() -> new NotFoundException("salutation.uuid.not.existing")));
        }
        else {
            destTblExternalPerson.setSalutation(null);
        }

        if( sourceDto.getPersonTypeUuid() != null ) {
            destTblExternalPerson.setRefPersonType( personTypeRepository
                    .findByUuid(sourceDto.getPersonTypeUuid())
                    .orElseThrow(() -> new NotFoundException("person.type.uuid.not.existing")));
        }
        else {
            destTblExternalPerson.setRefPersonType(null);
        }

        destTblExternalPerson.getContact().setNote(sourceDto.getContactNote());
        destTblExternalPerson.getContact().setAnonymized(sourceDto.getContactAnonymized());
    }
}
