/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service.util;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class SearchContactsFilterParams {
    private String contactType; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private UUID personTypeUuid; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private String searchText; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private String moduleName; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private boolean withoutModule; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private boolean expiringDataInPast; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private boolean delLockExceeded; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private boolean showAnonymized; // NOSONAR _fd: 10.02.2020 definitly used via Lombok-Accessors
    private boolean withSyncError; // NOSONAR
}
