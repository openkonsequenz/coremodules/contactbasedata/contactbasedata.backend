/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.contactbasedata.constants;

public final class Constants {
    public static final String PERSON_TYPE_UUID_NOT_EXISTING = "personType.uuid.not.existing";
    public static final String ADDRESS_TYPE_UUID_NOT_EXISTING = "addressType.uuid.not.existing";
    public static final String CONTACT_UUID_NOT_EXISTING = "contact.uuid.not.existing";
    public static final String MODULE_ASSIGNMENT_UUID_NOT_EXISTING = "module.assignment.uuid.not.existing";
    public static final String ANONYMOUS_TAG = "***";
    public static final String ADDRESS_UUID_NOT_EXISTING = "address.uuid.not.existing";
    public static final String COMMUNICATION_UUID_NOT_EXISTING = "communication.uuid.not.existing";
    public static final String COMMUNICATION_TYPE_UUID_NOT_EXISTING = "communication.type.uuid.not.existing";
    public static final String COMMUNICATION_TYPE_ALREADY_EXISTING_FOR_CONTACT = "communication.type.already.existing.for.contact";
    public static final String SALUTATION_UUID_NOT_EXISTING = "salutation.uuid.not.existing";

    private Constants() {
        // empty Constructor for the sake of SONAR
    }
    public static final String DB_VERSION_NOT_PRESENT = "DB-Version-not_present";
    public static final String CONTACT_TYPE_CONTACT_PERSON = "1CP";
    public static final String CONTACT_TYPE_EXTERNAL_PERSON = "2EP";
    public static final String CONTACT_TYPE_INTERNAL_PERSON = "3IP";
    public static final String CONTACT_TYPE_COMPANY = "4CO";
}
