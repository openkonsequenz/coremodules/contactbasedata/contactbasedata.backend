/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.SalutationService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.SalutationDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class SalutationControllerTest {

    @MockBean
    private SalutationService saluationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnAllSalutations() throws Exception {
        List<SalutationDto> sds = MockDataHelper.mockSalutationsDtos();

        when(saluationService.findAllSalutations()).thenReturn(sds);

        mockMvc.perform(get("/salutations"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnSingleSalutation() throws Exception {
        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();

        when(saluationService.getSalutationByUuid(any(UUID.class))).thenReturn(salutationDto);

        mockMvc.perform(get("/salutations/37454f86-2006-11ea-978f-2e728ce88125"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldInsertSalutation() throws Exception {
        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();
        salutationDto.setType("Master");
        salutationDto.setDescription("of the universe");

        when(saluationService.insertSalutation(any(SalutationDto.class)))
                .thenReturn(salutationDto);

        mockMvc.perform(post("/salutations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(salutationDto)))
                .andExpect(jsonPath("$.type", Matchers.is("Master")))
                .andExpect(jsonPath("$.description", Matchers.is("of the universe" )));
    }

    @Test
    void shouldUpdateSalutation() throws Exception {
        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();
        salutationDto.setUuid(UUID.randomUUID());

        when(  saluationService.updateSalutation(any(SalutationDto.class))).thenReturn(salutationDto);

        mockMvc.perform(put("/salutations/{uuid}", salutationDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(salutationDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateSalutationDueToError() throws Exception {
        SalutationDto salutationDto = MockDataHelper.mockSalutationDto();
        salutationDto.setUuid(UUID.randomUUID());

        when(  saluationService.updateSalutation(any(SalutationDto.class))).thenReturn(salutationDto);

        // use different UUIDs for path and object
        mockMvc.perform(put("/salutations/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(salutationDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeleteSalutation() throws Exception {
        mockMvc.perform(delete("/salutations/05ff2344-20a5-11ea-978f-2e728ce88125")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

}